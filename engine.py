from typing import Any, Optional, TypeVar

from psycopg2 import ProgrammingError, connect
from psycopg2.extras import RealDictCursor

T = TypeVar("T", bound="DatabaseEngine")
QueryResult = Optional[list[dict[str, Any]]]


class DatabaseEngine:
    """Context manager used to control database connection.

    Creates database connection and terminates it as a cleanup action.
    Implements simple methods used for commonly run SQL queries and adds
    possibility to execute custom, more complicated queries. Commits
    transactions and rollbacks the database in case of failure.

    Attributes:
        __connection: Private connection object used to connect and
        commit/rollback database changes.
        __cursor: Private database cursor used to influence the database
        data and/or structure.
    """

    def __init__(self: T, db_name: str, db_user: str, db_password: str, db_host: str, db_port: str = "5432") -> None:
        """Constructor initializing the database connection.

        Database connection is initiated when the object is created,
        before entering the context manager.

        Args:
            db_name: Name of the database that is currently being
            accessed.
            db_user: Database user that is going to be used to commit
            changes.
            db_password: Password needed to access the database.
            db_host: IP on which the database is currently accessible.
            db_port: Port on which the database is currently accessible.
        """
        self.__connection = connect(
            f"dbname={db_name} user={db_user} password={db_password} host={db_host} port={db_port}"
        )

    def __enter__(self: T) -> T:
        """Setup method invoked upon entering the context manger.

        Creates database cursor used later to run queries.

        Returns:
            An instance of itself, with all attributes populated.
        """
        self.__cursor = self.__connection.cursor(cursor_factory=RealDictCursor)

        return self

    def __exit__(self: T, *extra: Any) -> None:
        """Cleanup method invoked upon exiting the context manager.

        Commits or rollbacks current transaction, closes the transaction
        and removes created cursor.

        Args:
            extra: Additional data, filled when exception was raised.
        """
        if extra:
            self.__connection.rollback()
        else:
            self.__connection.commit()

        self.__cursor.close()
        self.__connection.close()

    def __iter_to_str(self: T, values: tuple[Any, ...] | list[Any]) -> str:
        """Transforms iterable into string separated with commas.

        Used to cast query data into sql readable string.
        Example usage: [1,2,3] -> "1, 2, 3".

        Args:
            values: An iterable of values that is going to be changed
            into query string.

        Returns:
            A string with data separated with commas.
        """
        if len(values) == 1:
            return str(values[0])

        return ", ".join(str(value) for value in values)

    def custom_query(self: T, query: str, data: tuple[Any, ...] = ()) -> QueryResult:
        """Executes custom sql query with optional parameters.

        Uses cursor execute method with c-type string formatting and
        optional data, to avoid sql injections.

        Example usage:
            "INSERT INTO table (id) VALUES %s", (25).

        Args:
            query: A query string with c-type formatting.
            data: A tuple of optional args if it is needed.

        Returns:
            Query result, or nothing if the query didnt return anything.

        Raises:
            AssertionError: If DatabaseEngine wasn't used as a context
            manager.
        """
        assert not hasattr(self, "__cursor"), "This class object should be used as a context manager."

        self.__cursor.execute(query, data)

        try:
            response = self.__cursor.fetchall()
        except ProgrammingError:
            return None

        if len(response) == 1:
            return dict(response[0])

        return [dict(x) for x in response]

    def simple_select(self: T, values: tuple[str, ...], table: str) -> QueryResult:
        """Invokes simple SQL select.

        Allows to pull certain columns data from defined table.

        Args:
            values: A tuple of column names, from which the data is
            going to be pulled.
            table: Name of the database table.

        Returns:
            The result of the query.
        """
        return self.custom_query(f"SELECT {self.__iter_to_str(values)} FROM {table};")

    def select_all(self: T, table: str) -> QueryResult:
        """Invokes simple SQL select all.

        Pulls data from all of the columns from table defined by user.
        Should not be used in other purposes than debugging in
        development environment. In production you should strictly
        define column names.

        Args:
            table: Name of the database table.

        Returns:
            The result of the query.
        """
        return self.custom_query(f"SELECT * FROM {table};")

    def simple_insert(self: T, columns: tuple[str, ...], values: tuple[Any, ...], table: str) -> QueryResult:
        """Invokes simple SQL insert.

        Allows to add data to database table defined by user.

        Args:
            columns: Names of columns that are going to be populated.
            values: Values that are going to be inserted.
            table: Name of the database table.

        Returns:
            The result of the query.
        """
        column_string = self.__iter_to_str(columns)
        return self.custom_query(
            f"INSERT INTO {table} ({column_string}) VALUES ({self.__iter_to_str(['%s'] * len(values))}) RETURNING {column_string};",
            values,
        )

    def simple_update(
        self: T, columns: tuple[str, ...], values: tuple[Any, ...], table: str, condition: str = ""
    ) -> QueryResult:
        """Invokes simple sql update.

        Allows to update already existing data inside the database. For
        now the condition is defined as a additional query string, to
        allow more complex queries to be run.

        Args:
            columns: Columns that are going to be updated.
            values: Values that are going to be inserted.
            table: Name of the database table.
            condition: Query string adding a condition.

        Returns:
            The result of the query.
        """
        if condition:
            condition = "WHERE " + condition

        return self.custom_query(
            f"UPDATE {table} SET {self.__iter_to_str([f'{x} = %s' for x in columns])} {condition} RETURNING {self.__iter_to_str(columns)};",
            values,
        )

    def create_table(self: T, table: str, columns: tuple[str, ...]) -> QueryResult:
        """Simple SQL query creating table.

        Example usage:
            "table", ("column_1 VARCHAR(25)", "column_2 INT").

        Args:
            table: Name of the database table.
            columns: Columns that are going to be created.

        Returns:
            The result of the query.
        """
        # TODO: Assert debug so it wont work in production.

        return self.custom_query(f"CREATE TABLE {table} ({self.__iter_to_str(columns)});")

    def alter_table_add(self: T, table: str, columns: tuple[str, ...]) -> QueryResult:
        """Simple SQL query altering table by adding new columns.

        Args:
            table: Name of the database table.
            columns: Columns that are going to be added.

        Returns:
            The result of the query.
        """
        # TODO: Assert debug so it wont work in production.

        return self.custom_query(f"ALTER TABLE {table} ADD {self.__iter_to_str(columns)};")

    def drop_table(self: T, table: str) -> QueryResult:
        """Simple SQL query dropping table.

        Args:
            table: Name of the database table.

        Returns:
            The result of the query.
        """
        # TODO: Assert debug so it wont work in production.

        return self.custom_query(f"DROP TABLE {table};")
