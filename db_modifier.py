import logging

import typer

from .engine import DatabaseEngine

# TODO: This will be moved in the future.
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

app = typer.Typer()


# TODO: This is a temporary solution.
# TODO: Should be rewritten to work more automatically.
# TODO: Unwrapped commands can be used by factories.


@app.command()
def create_table(
    db_name: str,
    db_user: str,
    db_password: str,
    table: str,
    columns: list[str],
    db_host: str,
    db_port: str = "5432",
) -> None:
    """Typer command creating table in specified database."""
    with DatabaseEngine(db_name, db_user, db_password, db_host, db_port) as db:
        db.create_table(table, tuple(columns))

    logger.info(f"Table {table!r} created successfully!")


@app.command()
def drop_table(
    db_name: str,
    db_user: str,
    db_password: str,
    table: str,
    db_host: str,
    db_port: str = "5432",
) -> None:
    """Typer command dropping table in specified database."""
    with DatabaseEngine(db_name, db_user, db_password, db_host, db_port) as db:
        db.drop_table(table)

    logger.info(f"Table {table!r} dropped successfully!")


if __name__ == "__main__":
    app()
